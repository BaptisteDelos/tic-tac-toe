import sys
from tictactoe.application import Application


if __name__ == "__main__":
    application = Application(sys.argv)
    sys.exit(application.run())
