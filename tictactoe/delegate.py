from PySide2 import QtGui
from PySide2 import QtCore
from PySide2 import QtWidgets
from tictactoe.cell import Cell, CellType


HEIGHT = 100


class Delegate(QtWidgets.QStyledItemDelegate):
    def __init__(self, parent=None):
        QtWidgets.QStyledItemDelegate.__init__(self, parent)
        self.font = QtGui.QFont()
        self.metrics = QtGui.QFontMetrics(self.font)

    def paint(self, painter, option, index):
        cell = index.data(QtCore.Qt.UserRole)
        width = option.rect.right() - option.rect.left()
        height = option.rect.top() - option.rect.bottom()
        center_x = option.rect.left() + width / 2
        center_y = option.rect.top() - height / 2

        if cell.type is CellType.CROSS:
            # draw a cross
            painter.setPen(QtCore.Qt.red)
            painter.drawLine(
                option.rect.left() + width / 4,
                option.rect.top() - height / 4,
                option.rect.right() - width / 4,
                option.rect.bottom() + height / 4,
            )
            painter.drawLine(
                option.rect.right() - width / 4,
                option.rect.top() - height / 4,
                option.rect.left() + width / 4,
                option.rect.bottom() + height / 4,
            )
        elif cell.type is CellType.ROUND:
            # draw a circle
            painter.setPen(QtCore.Qt.blue)
            painter.drawEllipse(
                QtCore.QPoint(center_x, center_y), width / 4, width / 4,
            )

    def sizeHint(self, option, index):
        return QtCore.QSize(HEIGHT, HEIGHT)
