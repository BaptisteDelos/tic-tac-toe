from PySide2 import QtWidgets
from tictactoe.table_view import Table


class View(QtWidgets.QWidget):
    
    def __init__(self, application, model, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.model = model
        self.app = application

        self.table = Table(self.model)
        self.label = QtWidgets.QLabel("...")
        self.btn_reset = QtWidgets.QPushButton("reset")
        self.btn_reset.setFixedWidth(100)

        layout = QtWidgets.QGridLayout(self)
        layout.addWidget(self.table, 0, 0, 1, 2)
        layout.addWidget(self.label, 1, 0)
        layout.addWidget(self.btn_reset, 1, 1)

        self.table.clicked.connect(self._clicked)
        self.btn_reset.clicked.connect(self._btn_reset_clicked)

        self.resize(340, 360)

    def _btn_reset_clicked(self, index):
        self.model.reset_board()

    def _clicked(self, index):
        success = self.model.update_cell(index, self.app.get_current_player())

        if not success:
            return

        if self.app.check_victory():
            self.label.setText("Victory for player {current_player}".format(current_player=self.app.get_current_player()))
            return
        
        if self.app.check_board_filled():
            self.label.setText("Stalemate...")
            return
        
        self.label.setText("Game in progress, turn {turn_count}".format(turn_count=self.app.get_turn_count()))
        self.app.switch_player()
    
    def resize_table_view(self, row, column):
        self.table.cell_updated(row, column)