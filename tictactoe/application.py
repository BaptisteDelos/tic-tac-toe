from PySide2 import QtWidgets
from tictactoe.board_model import BoardModel
from tictactoe.qmodel import QModel
from tictactoe.view import View


class Application:

    def __init__(self, args, parent=None):
        self.pyside_app = QtWidgets.QApplication(args)
        self.board_model = BoardModel()
        self.q_model = QModel(self)
        self.view = View(self, self.q_model)
    
    def get_cell(self, row, column):
        return self.board_model.get_cell(row, column)
    
    def mark_cell(self, row, column):
        success = self.board_model.mark_cell(row, column)

        if success:
            self.view.resize_table_view(row, column)
        
        return success
    
    def get_row_count(self):
        return self.board_model.get_row_count()
    
    def get_column_count(self):
        return self.board_model.get_column_count()
    
    def get_current_player(self):
        return self.board_model.get_current_player()
    
    def switch_player(self):
        return self.board_model.switch_player()

    def check_victory(self):
        return self.board_model.check_victory()
    
    def check_board_filled(self):
        return self.board_model.is_filled()
    
    def get_turn_count(self):
        return self.board_model.get_turn_count()
    
    def reset_board(self):
        self.board_model.reset()
    
    def run(self):
        self.view.show()
        return self.pyside_app.exec_()