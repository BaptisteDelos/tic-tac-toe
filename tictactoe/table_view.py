from PySide2 import QtWidgets
from tictactoe.delegate import Delegate


class Table(QtWidgets.QTableView):
    def __init__(self, model, parent=None):
        self.model = model
        self.delegate = Delegate()

        QtWidgets.QTableView.__init__(self, parent)

        self.setModel(self.model)
        self.setItemDelegate(self.delegate)

        self.horizontalHeader().hide()
        self.verticalHeader().hide()
        self.resizeColumnsToContents()
        self.resizeRowsToContents()

    def cell_updated(self, row, column):
        self.resizeRowToContents(row)
        self.resizeColumnToContents(column)