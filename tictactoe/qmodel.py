from PySide2 import QtCore


class QModel(QtCore.QAbstractTableModel):
    
    def __init__(self, application, parent=None):
        QtCore.QAbstractTableModel.__init__(self, parent)
        self.application = application

    def rowCount(self, parent=None):
        return self.application.get_row_count()

    def columnCount(self, parent=None):
        return self.application.get_column_count()

    def data(self, index, role):
        cell = self.application.get_cell(index.row(), index.column())

        if role == QtCore.Qt.UserRole:
            return cell

    def setData(self, index=None, value=None, role=None):
        if role == QtCore.Qt.EditRole:
            success = self.application.mark_cell(index.row(), index.column())
            self.dataChanged.emit(index, index)
            return success

        return False

    def update_cell(self, index, cell_type):
        return self.setData(index, cell_type, QtCore.Qt.EditRole)

    def reset_board(self):
        self.application.reset_board()
        self.dataChanged.emit(
            self.index(0, 0),
            self.index(self.rowCount() - 1, self.columnCount() - 1)
        )

    def flags(self, index):
        return (
            QtCore.Qt.ItemIsEnabled
            | QtCore.Qt.ItemIsSelectable
        )

    def get_app(self):
        return self.application

