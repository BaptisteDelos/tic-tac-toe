from enum import Enum


class CellType(Enum):
    CROSS = 0
    ROUND = 1
    BLANK = 2


class Cell:

    def __init__(self, cell_type=CellType.BLANK):
        self.type = cell_type
    
    def __repr__(self):
        return '<Cell({type})>'.format(type=self.type.name)