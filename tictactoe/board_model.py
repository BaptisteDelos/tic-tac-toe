from tictactoe.cell import Cell, CellType


class BoardModel:

    def __init__(self, size=3):
        self.data = list()
        self._size = 3
        self.current_player = CellType.ROUND
        self.turn_count = 0
        self._init_data()

    def _init_data(self):
        self.data = list()

        for row in range(self._size):
            self.data.append(list())
            
            for column in range(self._size):
                self.data[row].append(Cell())

    def get_row_count(self):
        return len(self.data)

    def get_column_count(self):
        return len(self.data[0])

    def get_turn_count(self):
        return self.turn_count

    def get_current_player(self):
        player = self.current_player
        return player

    def get_cell(self, row, column):
        return self.data[row][column]

    def mark_cell(self, row, column):
        invalid_edit = self.data[row][column].type != CellType.BLANK
        invalid_edit |= self.check_victory()
        invalid_edit |= self.is_filled()

        if invalid_edit:
            return False

        self.data[row][column] = Cell(self.current_player)
        self.turn_count += 1

        return True

    def switch_player(self):
        self.current_player = CellType(1 - self.current_player.value)

    def check_victory(self):
        victory = False

        # Check victory in rows and columns
        for i in range(self._size):
            victory_in_row = True
            victory_in_col = True

            for j in range(1, self._size):
                victory_in_row &= self.data[i][j].type == self.data[i][j - 1].type
                victory_in_col &= self.data[j][i].type == self.data[j - 1][i].type

            victory_in_row &= self.data[i][0].type != CellType.BLANK
            victory_in_col &= self.data[0][i].type != CellType.BLANK
            victory |= victory_in_row | victory_in_col

        # Check victory in diagonals
        diag_top_left_victory = True
        diag_top_right_victory = True

        for i in range(1, self._size):
            diag_top_left_victory &= (
                self.data[i][i].type == self.data[i - 1][i - 1].type
            )
            diag_top_right_victory &= (
                self.data[i][self._size - i - 1].type
                == self.data[i - 1][self._size - i].type
            )

        victory |= (diag_top_left_victory | diag_top_right_victory) & (
            self.data[1][1].type != CellType.BLANK
        )

        return victory

    def is_filled(self):
        return self.turn_count == self._size * self._size
    
    def reset(self):
        self._init_data()
        self.turn_count = 0
        self.current_player = CellType.ROUND

    def __repr__(self):
        repr_str = "<BoardModel("

        for row in self.data:
            repr_str += "\n\t[".ljust(5)

            for cell in row:
                cell_str = "{}".format(cell).ljust(15)
                repr_str += cell_str

            repr_str += "]"
        repr_str += ")>"

        return repr_str